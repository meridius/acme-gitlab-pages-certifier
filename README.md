# ACME GitLab Pages Certifier
Wrapper for great CLI tool [rolodato/gitlab-letsencrypt][gitlab-letsencrypt].

The CLI tool allows you to generate a [Let's Encrypt][pages] certificate for use with [GitLab Pages][pages].  
This wrapper shields you from installing NPM and the tool and optionally provides you with a way to easily renew issued certificates. 

All you need is [Docker][docker].

## How

- install [Docker][docker]
- download this repo
- run `./certify.sh` to issue a certificate for your Pages
  - help will be shown for none or invalid arguments; you can also look at [documentation][gitlab-letsencrypt]

## Easy certificate renewal
### Save config
After you've successfully issued the certificate, you'll be asked to **optionally** let the `certify.sh` script save the provided CLI values to a file. This is to shield you from remembering all the config options and their values, especially if you have multiple Pages (with different structure).

All config files will reside in `./user_data/` directory. Their names are generated from stated repository name. E.g. for the repo `me/my-repo` the file name will be `me_my-repo`. Multiple configurations can be saved this way.

*Please note that the generated file contains all the necessary values you provided to the CLI tool, including access token for your repository. Do not save the file if you don't believe you can protect it. You have been warned. ;-)*

### Renew license
After you've saved the config, you can use it via `renew.sh` script. It will ask you for the location of the config file you want to use, let's you verify the loaded values and will run the certificate renewal process.

Other way to renew the certificate is to simply run `certify.sh` again with the same values you provided the first time.



[docker]: https://www.docker.com/get-docker
[pages]: https://pages.gitlab.io/
[le]: https://letsencrypt.org/
[gitlab-letsencrypt]: https://github.com/rolodato/gitlab-letsencrypt
