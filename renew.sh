#!/usr/bin/env bash

source ./src/common.sh

_construct_arguments() {
    arguments="
        --repository $1
        --email $2
        --token $3
        --domain $4
        --path $5
    "

    if [[ $6 -eq 1 ]]; then
        arguments="${arguments} --jekyll"
    fi

    if [[ $7 -eq 1 ]]; then
        arguments="${arguments} --staging"
    fi

    echo "${arguments}"
}

_warn_no_args() {
    data_dir="$1"
    echo "Specify path to a file with configuration."

    if [[ `ls -1q ${data_dir} | wc -l` -gt 0 ]]; then
        echo "Or provide one previously saved from ${data_dir}:"
        ls -1 ${data_dir}
    fi

    exit 1
}

if [[ $# -eq 0 ]]; then
    _warn_no_args "${DATA_DIR}"
    exit 1
elif [[ ! -f "$1" ]] ; then
    echo "File $1 does not exist."
    _warn_no_args "${DATA_DIR}"
    exit 1
fi

source "$1"

echo -e "Configuration from $1 was loaded successfully\n"
echo -e "repository:\t${REPOSITORY}"
echo -e "email:\t\t${EMAIL}"
echo -e "token:\t\t${TOKEN}"
echo -e "domain:\t\t${DOMAIN}"
echo -e "path:\t\t${CHALLENGE_PATH}"
echo -e "jekyll:\t\t${JEKYLL}"
echo -e "staging:\t${STAGING}\n"

read -n 1 -p "Renew certificate with these values? [Y/n] " yn
case ${yn} in
    Y|y|"")
        arguments=`_construct_arguments \
            "${REPOSITORY}" \
            "${EMAIL}" \
            "${TOKEN}" \
            "${DOMAIN}" \
            "${CHALLENGE_PATH}" \
            "${JEKYLL}" \
            "${STAGING}"`
        _run_docker "${arguments}" || exit 1
        ;;
    N|n)
        exit 0
        ;;
    *)
        exit 0
        ;;
esac

exit 0
