#!/usr/bin/env bash

source ./src/common.sh

_save_user_data() {
    email=""
    domain=""
    repository=""
    token=""
    challenge_path=""
    jekyll=0
    staging=0

    while [[ $# -gt 0 ]]; do
        case $1 in
            --email)
                email="$2"
                shift 2
                ;;
            --domain)
                domain="$2"
                shift 2
                while [[ ! $1 =~ ^-- ]]; do
                    domain="${domain} $1"
                    shift
                done
                ;;
            --repository)
                repository="$2"
                shift 2
                ;;
            --token)
                token="$2"
                shift 2
                ;;
            --path)
                challenge_path="$2"
                shift 2
                ;;
            --jekyll)
                jekyll=1
                shift
                ;;
            --staging)
                staging=1
                shift
                ;;
        esac
    done

    formatted_data="REPOSITORY=${repository}
EMAIL=${email}
TOKEN=${token}
DOMAIN=\"${domain}\"
CHALLENGE_PATH=${challenge_path}
JEKYLL=${jekyll}
STAGING=${staging}"

    file_path="${DATA_DIR}/${repository//\//_}"
    echo "${formatted_data}" > "${file_path}"
    echo "The configuration was saved to ${file_path} file."
    echo "Use 'renew.sh' to renew the certificate when it is about to expire."
}

_run_docker "$@" || exit 1

echo
read -n 1 -p "Would you like to save provided configuration for easy certificate renewal?
Please note that it will contain your token in plaintext form. [Y/n] " yn
case ${yn} in
    Y|y|"")
        echo
        _save_user_data "$@"
        ;;
    N|n)
        exit 0
        ;;
    *)
        exit 0
        ;;
esac
