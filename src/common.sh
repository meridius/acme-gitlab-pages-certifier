#!/usr/bin/env bash

PROJECT_NAME=acme-gitlab-pages-certifier
DATA_DIR=./user_data

_build_docker() {
    # build if it does not exist
    if [[ "$(docker images -q ${PROJECT_NAME}:latest 2> /dev/null)" == "" ]]; then
        docker build -t ${PROJECT_NAME} . || exit 1
    fi
}

_run_docker() {
    arguments="$@"

    _build_docker || exit 1

    docker run -it --rm \
        ${PROJECT_NAME} \
        ${arguments} \
        || exit 1
}

_run_in_docker() {
    entrypoint="$1"
    shift
    arguments="$@"

    _build_docker || exit 1

    docker run --entrypoint "$entrypoint" --rm \
        ${PROJECT_NAME} \
        ${arguments} \
        || exit 1
}
